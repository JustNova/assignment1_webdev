'use strict';
//loading js after the page has been loaded and calling the main function
window.addEventListener('DOMContentLoaded', main);
//main function will create a val variable that will store ell the element with the listen class getElementsByTagName
//then a for loop will add eventListener to every tag with said class name and then call the generateTable function
function main() {
  let val = document.getElementsByClassName("listen");
  
  for(let i = 0; i < val.length; i++) {
   val[i].addEventListener("input", generateTable);
  }
  generateTable();
};
//the generate table function takes care of everything needed for the table generation
function generateTable() {
  //these variable declarations store the value that is inside the input tags to create 
  //the inital table that will load  with the page
  //the stringBuilder variable is an empty string that will later dynamically get built in the for loops witht the table
  let tableStringBuilder = ""; 
  let rowNum = document.querySelector("#row-count").value;
  let colNum = document.querySelector("#col-count").value;
  let tableWidth = document.querySelector("#table-width").value;
  let txtColor = document.querySelector("#text-color").value;
  let bgColor = document.querySelector("#background-color").value;
  let borderWidth = document.querySelector("#border-width").value;
  let borderColor = document.querySelector("#border-color").value;

  //the oldTable function will simply store the element with the tag name table at position 0
  let oldTable = document.getElementsByTagName("table")[0];
  //creating the first opening table tag by appending it and adding it to the stringBuilder
  let table = document.createElement("table");
  tableStringBuilder += "<table>"
  changeTagWidth(table, `${tableWidth}%`); //using the functions from utilisties to set the css for the table
  changeTagTextColor(table, txtColor);
  changeTagBgColor(table, bgColor);
  let tbody = document.createElement("tbody") //adding the tbody to the table addint onto the stringBuilder
  tableStringBuilder += "\n\t<tbody>"
  for (let i = 0; i < rowNum; i++) { //nested for loops that will take care of creating tr and td tags depending on the rowNum 
    let row = document.createElement("tr"); //and the colNum for the amount of iterations and adding those onto the stringBuilder
    tableStringBuilder += "\n\t\t<tr>"
    for (let j = 0; j < colNum; j++) {
      tableStringBuilder += "\n\t\t\t<td>"
      tableStringBuilder += (" "+`td ${i}, ${j}`); //
      let col = document.createElement("td");
      let textInTable = document.createTextNode(`td ${i}, ${j}`)
      col.style.border = `${borderWidth}px solid ${borderColor}`;
      col.appendChild(textInTable);
      row.appendChild(col);
      tableStringBuilder +=  " </td>" // adding closing tags for everything 
    }
    tbody.appendChild(row);
    tableStringBuilder += "\n\t\t</tr>" 
  }
  table.appendChild(tbody);
  tableStringBuilder += "\n\t</tbody>"
  tableStringBuilder += "\n</table>"
  document.querySelector("#table-render-space").appendChild(table); // appending the table to the element with the id table-render-space and
  document.querySelector("#table-html-area").value = tableStringBuilder; // setting the value of the textarea to the stringBuilder
  if (!((typeof oldTable) === undefined)) { //checking if the table is not undefined on function call and
    oldTable.remove();                      // deleting it so the new one can take its place
  }
};
