'use strict';

/**
 * Function to change the width of a given element
 * @param {HTMLElement} el HTML element of which we want to change the width
 * @param {String} width String containing the width in percentages that we want to set
 */
 function changeTagWidth(el, width) {
  el.style.width = width;
}

/**
 * Function to change the font color of an element
 * @param {HTMLElement} el HTML element of which we want to change the color
 * @param {string} color String containing the color we want the text to be set to
 */
function changeTagTextColor(el, color) {
    el.style.color = color;
}

/**
 * Function to change the background color of the border of an element
 * @param {HTMLElement} el HTML element of which we need to change the background color
 * @param {string} bgc String containing the background color to set
 */
function changeTagBgColor(el, bgc) {
    el.style.background = bgc;
}

/**
 * Function to change the width of the border of a given element
 * @param {HTMLElement} el HTML element of which to change the width of the border
 * @param {String} width String containing the width in pixels that we want to set
 */
 function changeTagBorderWidth(el, width) {
  el.style.borderWidth = width;
}

/**
 * Function to change the color of the border of a given element
 * @param {HTMLElement} el HTML element of which to change the color of the border
 * @param {String} color String containing the color we want the border to be
 */
function changeTagBorderColor(el, color) {
    el.style.borderColor = color;
}

/**
 * Function that will set input attributes to given values
 * @param {HTMLElement} el HTML element on which we need to set to the given attributes
 * @param {string[]} attr String array containing the names of the attributes we need to set
 * @param {any[]} vals Array contaning values of the attributes in the attr array, each value can be either a number value or a string
 */
function setMultipleAttributes(el, attr, vals) {
    for (let i = 0; i < attr.length; i++) {
        el.setAttribute(attr[i], vals[i]);
    }
}